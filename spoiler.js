
Drupal.behaviors.spoiler = function() {
  $('.spoiler')
    .children('.spoiler-content')
    .addClass('spoiler-content-js')
    .removeClass('spoiler-content')
    .siblings('.spoiler-toggle').children('span').remove();
    
   $('.spoiler')
    .children('.spoiler-toggle').click(function() {
      $(this)
      .parent().children('.spoiler-content-js').toggle('normal');
    });
};
